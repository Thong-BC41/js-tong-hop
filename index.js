//bài 1:

function inBangSo() {
  var contentHTML = "";
  for (var number = 1; number <= 100; number++) {
    if (number % 10 == 0) {
      var contentr = `${number}<br>`;
      contentHTML += contentr;
    } else {
      contentr = `${number} `;
      contentHTML += contentr;
    }
  }
  document.getElementById("tangdan").innerHTML = contentHTML;
}

//bài 2
var array = [];
function nhapSo() {
  var a = document.getElementById("nhapso").value * 1;
  array.push(a);
  document.getElementById("themso").innerHTML = array;
}

function kiemTraSoNguyenTo(number) {
  var checksnt = true;
  if (number < 2) {
    checksnt = false;
  } else {
    for (var i = 2; i < number - 1; i++) {
      if (number % i == 0) {
        checksnt = false;
        break;
      }
    }
  }
  return checksnt;
}

function soNguyenTo(array) {
  var arrsnt = [];
  for (var i = 0; i < array.length; i++) {
    var checksnt = kiemTraSoNguyenTo(array[i]);
    if (checksnt == true) {
      arrsnt.push(array[i]);
    }
  }
  document.getElementById("soNguyenTo").innerHTML = arrsnt;
}

//bài 3

function tinhS() {
  var n = document.getElementById("sothu1").value * 1;
  var S = 0;
  var sum = 0;
  if (n < 2) {
    S = " Số nhập phải >=2";
  } else {
    for (var i = 2; i <= n; i++) {
      sum += i;
    }
    S = sum + 2 * n;
  }
  document.getElementById("tinhtong").innerHTML = S;
}

//bài 4:
function tinhuoc() {
  uoc = [];
  var number4 = document.getElementById("nhapso4").value * 1;
  for (var i = 0; i <= number4; i++) {
    if (number4 % i == 0) {
      uoc.push(i);
    }
  }
  document.getElementById("uoc").innerHTML = uoc;
}

//bài 5

function daonguoc() {
  var number5 = document.getElementById("nhapso5").value * 1;
  var reverse = 0;
  if (number5 > 0 && number5 % 1 == 0) {
    number5 = number5 + "";
    reverse = number5.split("").reverse().join("");
  } else {
    reverse = `Không phải là số nguyên dương`;
  }
  document.getElementById("daonguoc").innerHTML = reverse;
}

//bài 6:

function timX() {
  var sum = 0;
  var x = [];
  for (var i = 0; i < 100; i++) {
    sum = sum + i;
    if (sum <= 100) {
      x.push(i);
    }
  }
  document.getElementById("timx").innerHTML = x.pop();
}

//bài 7:
//Cách 1:
// function bangCuuChuong(){
//   var number7=document.getElementById('nhapso7').value*1
//   document.getElementById('bangcuuchuong').innerHTML=`${number7} x 0 = ${number7*0} <br>
//   ${number7} x 1 = ${number7*1}<br>
//   ${number7} x 2 = ${number7*2}<br>
//   ${number7} x 3 = ${number7*3}<br>
//   ${number7} x 4 = ${number7*4}<br>
//   ${number7} x 5 = ${number7*5}<br>
//   ${number7} x 6 = ${number7*6}<br>
//   ${number7} x 7 = ${number7*7}<br>
//   ${number7} x 8 = ${number7*8}<br>
//   ${number7} x 9 = ${number7*9}<br>
//   ${number7} x 10 = ${number7*10}
//   `
// }
//Cách 2:
function bangCuuChuong() {
  var contentHTML = "";
  var number7 = document.getElementById("nhapso7").value * 1;
  for (i = 1; i <= 10; i++) {
    var contenTr = `<tr>${number7} x ${i} = ${number7 * i}</tr> <br>`;
    contentHTML += contenTr;
  }
  document.getElementById("bangcuuchuong").innerHTML = contentHTML;
}

//bài 8:

function dealCards(players, cards) {
  for (var i = 0; i < cards.length; i++) {
    var playerIndex = i % players.length;
    players[playerIndex].push(cards[i]);
  }
  return players;
}

function chiabai() {
  var players = [[], [], [], []];
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  result = dealCards(players, cards);
  document.getElementById("chiabai").innerHTML = `<br>player1 : ${result[0]}<br>
                                                      player2 : ${result[1]}<br>
                                                      player3 : ${result[2]}<br>
                                                      player4 : ${result[3]}`;
}

//bài 9
function tinhgacho() {
  var gc = document.getElementById("tonggacho").value * 1;
  var chan = document.getElementById("tongsochan").value * 1;
  var cho = chan / 2 - gc;
  var ga = gc - cho;
  if (ga > 0 && ga % 1 == 0 && cho > 0 && cho % 1 == 0) {
    document.getElementById(
      "gacho"
    ).innerHTML = `Số gà là ${ga}, số chó là ${cho}`;
  } else {
    document.getElementById("gacho").innerHTML = "Số gà chó không hợp lệ";
  }
}

//bài 10: tính góc lệch, độ lệch phút so giờ

function goc() {
  var gio = document.getElementById("sogio").value * 1;
  var phut = document.getElementById("sophut").value * 1;
  if (gio >= 0 && gio <= 12 && phut >= 0 && phut <= 60) {
    var d = phut * 6 - (gio * 30 + phut * 0.5);
    document.getElementById(
      "tinhgoc"
    ).innerHTML = `Góc lệch ${gio}:${phut} có độ lệch ${d}	°`;
  } else {
    document.getElementById("tinhgoc").innerHTML = `Giờ phút không hợp lệ `;
  }
}
